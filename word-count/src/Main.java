import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<>();
        try {

            System.out.println("Project 1: Word Statistics");
            Scanner input = new Scanner(new File(args[0]));
            map = wordStats(input);
            System.out.println(map.toString());
            input.close();
            System.out.println("Lines: "+lineCount(args[0]));
            System.out.println("Chars: "+charCount(args[0]));


        }catch(Exception e){
            System.out.println("Exception" + e.getMessage());
        }
    }


    public static Map<String, Integer> wordStats(Scanner input){
        Map<String, Integer> map = new HashMap<>();

        while(input.hasNext()) {
            String word = input.next().toLowerCase().replaceAll("[^a-zA-Z ]", "");

            Integer count = map.get(word);
            count = (count == null) ? 1 : ++count;
            map.put(word, count);

        }

        return map;
    }

    public static int lineCount(String args) throws Exception{

        Scanner input = new Scanner(new File(args));

        int lines = 0;
        while (input.hasNextLine()){

            lines++;
            input.nextLine();
        }

        input.close();
        return lines;
    }

    public static int charCount(String args) throws Exception{

        Scanner input = new Scanner(new File(args));

        int chars = 0;
        while (input.hasNextLine()) {

            chars += input.nextLine().length();

        }

        input.close();
        return chars;
    }



}